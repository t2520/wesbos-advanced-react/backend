// This has changed to "keystone-6/core" look into this and use the new on in the real app.
import { createAuth } from "@keystone-next/auth";
import { config, createSchema } from "@keystone-next/keystone/schema";
import {
  withItemData,
  statelessSessions,
} from "@keystone-next/keystone/session";
import { User } from "./schemas/User";
import { Product } from "./schemas/Products";
import { ProductImage } from "./schemas/ProductImage";

import "dotenv/config";
import { insertSeedData } from "./seed-data";

const databaseURL =
  process.env.DATABASE_URL || "mongodb://localhost/keystone-sick-fits-tutorial";

//   Generate a cookie for user login.
const sessionConfig = {
  maxAge: 60 * 60 * 24 * 360, // How long does user stay logged in.
  secret: process.env.COOKIE_SECRET,
};

const { withAuth } = createAuth({
  // Specify which list holds the values for authentication
  listKey: "User",
  // Specify which field in user will identify the user.
  identityField: "email",
  secretField: "password",
  // When there is no users this allows to setup initial user
  initFirstItem: {
    fields: ["name", "email", "password"],
    // TODO: Add initial roles here. Set the 1st user created with initFirstItem
    // as admin.
  },
});

// Wrap config in withAuth to get authentication to work.
export default withAuth(
  config({
    server: {
      cors: {
        origin: [process.env.FRONTEND_URL],
        // pass on the cookie
        credentials: true,
      },
    },
    db: {
      adapter: "mongoose",
      url: databaseURL,
      //  We can hook into OnConnect function and inject sample / seed data into the database.
      async onConnect(keystone) {
        // when we run the dev backend data wit npm run seed-data, it runs the
        // script in the package.json that runs the command "keystone-next
        // --seed-data" which combined with if check below will inject the seed
        // date straight into the database using the index.ts module that Wes
        // provided.  This will need to be adapted if using database other than
        // MongoDB.
        if (process.argv.includes("--seed-data")) {
          await insertSeedData(keystone);
        }
      },
    },
    // Keystone refers to our date types as lists, e.g. list of users, products etc.
    lists: createSchema({
      // Schema items go in here.

      // Can be written as just User as if the property name and the variable are
      // the same then this can be shortened. Using the full syntax makes the
      // import work though.
      // User schema imported above used below
      User,
      Product,
      ProductImage,
    }),
    // Give access to users to the Keystone Admin UI.
    ui: {
      //  show the Ui only for people who pass this test.  The user session will be passed to us.
      isAccessAllowed: ({ session }) =>
        // console.log(session);
        // The ? will return true they are logged in if there is session and
        // data otherwise they are not logged in.  The !! is to fix a Unsafe
        // return on any typed value error
        // https://github.com/typescript-eslint/typescript-eslint/blob/v4.13.0/packages/eslint-plugin/docs/rules/no-unsafe-return.md
        // The !! turns the return into a boolean.
        !!session?.data,
    },
    // Add a session for the user login where they are given a cookie which can
    // expire or expires on log out.  The withItemData allows use of extra
    // arguments like userId, email etc to the session so when we access the
    // session we also have access to this extra data
    // TODO: replace withItemData with sessionData for keystone 6.
    session: withItemData(statelessSessions(sessionConfig), {
      // GraphQL Query
      User: "id name email",
    }),
    // On object where we ask for
  })
);
